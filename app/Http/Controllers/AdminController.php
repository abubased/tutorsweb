<?php

namespace App\Http\Controllers;

use App\Page;
use App\about;
use App\General;
use Illuminate\Http\Request;


class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    	$general = General::find(1);

	     return view('admin.index', ['general' => $general] );
    }

    public function theme_options($value='')
    {

        $general = General::find(1);

         return view('admin.general', ['general' => $general] );
    }

    public function updateOption(Reauest $request)
    {

        $general = new General();

        // $general->logo          = request('logo');
        // $general->top_header    = request('top_header');
        // $general->email         = request('email');
        // $general->phone         = request('phone');
        // $general->menu_color    = request('menu_color');
        // $general->footer_bg     = request('footer_bg');

        if (request('logo')) {
            request('logo')->isValid([
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time().'.'.request('logo')->getClientOriginalExtension();
            $move = request('logo')->move(public_path('assets/images'), $imageName);

            if (!$move) {
                $logo = request('oldlogo');
            } else {
                $logo  = $imageName;
            }
        } else { $logo = request('oldlogo'); }



        $general->where('id', 1)->update([
                'logo'          => $logo,
                'top_header'    => request('top_header'),
                'email'         => request('email'),
                'menu_color'    => request('menu_color'),
                'footer_bg'     => request('footer_bg')

            ]);

        return redirect('/Admin/theme-options')->with('mssg', 'Theme Option Update Successfully');

    }

      public function about()
    {
        $about = Page::where('slug',"about")->first();
        return view('admin.about')->with(compact('about'));
    }


    public function editAbout(Request $request)
    {
        return view('admin.about', ['about' => $about] );
    }

    public function updateAbout()
    {

    	$about = Page::find(1);

	     return view('admin.about', ['about' => $about] );
    }

    public function addNewSubject(Request $request)
    {
        $data = [
          'subjectTitle' =>  $request['subject_name'],
          'type' =>  $request['type'],
          'languageMedium' =>  $request['language_medium'],
          'subjectCode' =>  $request['subject_code'],
          'url' =>  $request['url']
        ];
        $data['data'] = Subject::insert($data);
        return view('front.welcome');
    }

    public function home(Request $request)
    {
        # code...
    }

    // public function about(Request $request)
    // {
    //     # code...
    // }

    public function tutors(Request $request)
    {
        # code...
    }

    public function fees(Request $request)
    {
        # code...
    }

    public function howItWorks(Request $request)
    {
        # code...
    }

    public function contact(Request $request)
    {
        # code...
    }

    public function teach(Request $request)
    {
        # code...
    }

    public function learn(Request $request)
    {
        # code...
    }

}
