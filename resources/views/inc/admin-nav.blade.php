 <div class="page-content d-flex align-items-stretch">

        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="{{asset('assets/admin/img/avatar-1.jpg')}}" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">tutorsWeb</h1>
              <p>Learn from anywhere</p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
            <li class="active"><a href="/Admin"> <i class="icon-home"></i>Home </a></li>
            <li><a href=""> <i class="fa fa-cogs"></i>Theme Options </a></li>
            <li><a href="#subjectnav" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-bar-chart"></i>Subjects </a>
              <ul id="subjectnav" class="collapse list-unstyled ">
                <li><a href="#">All Subject</a></li>
                <li><a href="{{ route('addNewSubject') }}">Add New</a></li>
              </ul>
            </li>
            {{-- <li><a href="#tutorsnav" aria-expanded="false" data-toggle="collapse"> <i class="icon-padnote"></i>Tutors </a>
              <ul id="tutorsnav" class="collapse list-unstyled ">
                <li><a href="#">All Subject</a></li>
                <li><a href="{{ route('addNewSubject') }}">Add New</a></li>
              </ul>
            </li> --}}
            <li><a href="#pages" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Pages </a>
              <ul id="pages" class="collapse list-unstyled ">
                <li><a href="{{ route('editHome') }}">Home</a></li>
                <li><a href="{{ route('editTutors') }}">Tutors</a></li>
                <li><a href="{{ route('editLearn') }}">Learn</a></li>
                <li><a href="{{ route('editTeach') }}">Teach</a></li>
                <li><a href="{{ route('editAbout') }}">About</a></li>
                <li><a href="{{ route('editFees') }}">Fees</a></li>
                <li><a href="{{ route('editHowItWorks') }}">How it Works</a></li>
                <li><a href="{{ route('editContact') }}">Contact</a></li>
              </ul>
            </li>
            <!-- <li><a href="login.html"> <i class="icon-interface-windows"></i>Login page </a></li> -->
          </ul>
        <!--
          <span class="heading">Extras</span>
          <ul class="list-unstyled">
            <li> <a href="#"> <i class="icon-flask"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-screen"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-mail"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-picture"></i>Demo </a></li>
          </ul>
        -->

        </nav>
