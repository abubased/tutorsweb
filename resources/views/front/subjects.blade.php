@extends('layouts.bg_layout')

@section('head')
    <link rel="stylesheet" href="{{asset('DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection

@section('content')

  <style>
	  .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #007bff !important;
 background: unset !important;
}
  </style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <section class="inner-banner">
            <div class="container">
                <ul class="list-unstyled thm-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active"><a href="#">Subjects</a></li>
                </ul><!-- /.list-unstyled -->
                <!-- <h2 class="inner-banner__title"> </h2>  -->
                <div class="m-4"></div>
            </div><!-- /.container -->
        </section><!-- /.inner-banner -->

        <div class="m-5"></div>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col"> <!-- left sidebar  -->

                    <nav class="dashboard-sidebar">

                        <div class="list-unstyled" style="padding-left:15px !important">

							 <ul class="nav nav-pills" role="tablist" id="menu">
								<li class="nav-item">

								  <a class="nav-link active type-menu" data-toggle="pill" href="#all">All</a>
								</li>
								   @foreach($type as $each)
								   <li class="nav-item"><a class="nav-link type-menu" data-toggle="pill" href="#{{$each['type']}}">{{$each['type']}}</a>
								   </li>
								  @endforeach
							  </ul>

                        </div>
                    </nav>
                </div>
					</div>
					  <!-- Nav pills -->

 <div class="col-md-9">
  <!-- Tab panes -->
  <div class="tab-content">
    <div id="all" class="container tab-pane active"><br>
         <div id="datatable">
                            <table id="datatable_1" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Subject Title</th>
                                    <th>Type</th>
                                    <th>Language Medium</th>
                                    <!-- <th>Subject Code</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                <?php $No = 1?>
                                @foreach($data as $item)
                                    <tr>
                                        <td>{{$No++}}</td>
                                        <td><a href = "{{$item['url']}}" target="_blank">{{$item['subjectTitle']}}</a></td>
                                        <td>{{$item['type']}}</td>
                                        <td>{{$item['languageMedium']}}</td>
                                      <!--  <td>{{$item['subjectCode']}}</td> -->
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                    </div>
    </div>
	  @foreach($type as $each)
    <div id="{{$each['type']}}" class="container tab-pane fade"><br>
                        <div id="datatable">
                            <table id="datatable_1" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Subject Title</th>
                                    <th>Type</th>
                                    <th>Language Medium</th>
                                    <!-- <th>Subject Code</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                <?php $No = 1?>
                                @foreach($data as $item)
									@if($item['type']==$each['type'])
                                    <tr>
                                        <td>{{$No++}}</td>
                                        <td><a href = "{{$item['url']}}" target="_blank">{{$item['subjectTitle']}}</a></td>
                                        <td>{{$item['type']}}</td>
                                        <td>{{$item['languageMedium']}}</td>
                                      <!--  <td>{{$item['subjectCode']}}</td> -->
                                    </tr>
									@endif
                                @endforeach

                                </tbody>
                            </table>
                    </div>
    </div>
	  @endforeach

  </div>

 </div>

               </div>

        </section>

    <div class="m-5"></div>
@endsection

@section('foot')

    <script>
		console.log("test");



	</script>

    <script src="{{asset('DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('DataTables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/js/custom-datatable.js')}}"></script>
@endsection
