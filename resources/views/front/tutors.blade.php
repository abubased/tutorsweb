@extends('layouts.bg_layout')

@section('content')

<section class="inner-banner">
    <div class="container">
        <ul class="list-unstyled thm-breadcrumb">
            <li>
                <a href="#">Home</a></li>
            <li class="active">
                <a href="{{ route('tutors') }}">Tutors</a>
            </li>
        </ul><!-- /.list-unstyled -->
    </div><!-- /.container -->
    <div class="m-4 text-success">
        <h1>How to Apply as a New Funding Portal</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, totam consequuntur. Impedit, blanditiis dicta! Quasi, repellat itaque, incidunt eaque ipsum laudantium perferendis ipsam eveniet, fuga tempora magnam debitis inventore odit.</p>
        <img src="{{asset('assets/images/page.jpg')}}">
    </div>
</section><!-- /.inner-banner -->


</section>

@endsection
