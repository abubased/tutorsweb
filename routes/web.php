<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
/************************
 Admin Route Option
*************************/

Route::get('/Admin', 'AdminController@index');
Route::get('/Admin/theme-options', 'AdminController@theme_options');
Route::post('/Admin/theme-options', 'AdminController@updateOption');
Route::post('/Admin/home', 'AdminController@home')->name('editHome');
Route::get('/Admin/about', 'AdminController@about')->name('about');
Route::post('/Admin/about', 'AboutController@store')->name('editAbout');
Route::get('/Admin/edit/about', 'AboutController@edit');
Route::post('/Admin/edit/about', 'AboutController@update')->name('updateAbout');
Route::post('/Admin/tutors', 'AdminController@tutors')->name('editTutors');
Route::post('/Admin/fees', 'AdminController@fees')->name('editFees');
Route::post('/Admin/howitworks', 'AdminController@howItWorks')->name('editHowItWorks');
Route::post('/Admin/contact', 'AdminController@contact')->name('editContact');
Route::post('/Admin/teach', 'AdminController@teach')->name('editTeach');
Route::post('/Admin/learn', 'AdminController@learn')->name('editLearn');
Route::post('/add', 'AdminController@addNewSubject')->name('addNewSubject');


/************************
 Fontend Route Option
*************************/
Route::get('/', function () { 	return view('front.welcome'); })->name('homepage');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/howitworks', function () {     return view('front.howitwork'); });
Route::get('/tutors', function () {    return view('front.tutors'); })->name('tutors');
Route::get('/fee', function () {    return view('front.fee'); });
Route::get('/contact', function () {     return view('front.contact');});
Route::get('/learn', function () {     return view('front.learn');});
Route::get('/teach', function () {     return view('front.teach');});
Route::get('/subjects', 'HomeController@subjects' )->name('subject');

// Route::post('/addSubject', 'HomeController@addSubjects')->name('addSubject');

Route::get('/tutor-register', function () {     return view('users.tutor-register');});
Route::get('/student-register', function () {     return view('users.student-register');});
Route::get('/home', 'HomeController@index')->name('home');


/***********************
User Dashboard
************************/

Route::get('/student/profile', function () {     return view('users.student.profile');});
Route::get('/student/student-edit-account', function () {     return view('users.student.editaccount');});
Route::get('/student/lessons ', function () {     return view('users.student.courses');});
Route::get('/student/my-account ', function () {     return view('users.student.my-account');});
Route::get('/student/credit', function () {     return view('users.student.credit');});

Route::get('/tutor/my-account', function () {     return view('users.tutor.my-account');});
Route::get('/tutor/profile', function () {     return view('users.tutor.profile');});



//summernote form
Route::view('/summernote','summernote');

//summernote store route
Route::post('/summernote','SummernoteController@store')->name('summernotePersist');

//summernote display route
Route::get('/summernote_display','SummernoteController@show')->name('summernoteDispay');
